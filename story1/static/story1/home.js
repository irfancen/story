aboutID = document.getElementById("about");
profileID = document.getElementById("profile")

var showOnScroll = function() {
    var y = window.scrollY;
    if (y >= 450) {
        aboutID.className = "about show";
    }
    else {
        aboutID.className = "about hide";
    }
    if (y >= 1250) {
        profileID.className = "profile show";
    }
    else {
        profileID.className = "profile hide";
    }
};

window.addEventListener("scroll", showOnScroll);