from django.shortcuts import render, redirect
from story5.forms import CoursesForm
from story5.models import Courses

def index(request):
    data = Courses.objects.all()
    context = {
        "data" : data,
    }
    return render(request, "story5/index.html", context)

def add(request):
    form = CoursesForm(request.POST or None)
    if form.is_valid() and request.method == "POST":
        form.save()
        return redirect('/add')
    context = {
        "form" : form,
        "task" : "add",
    }
    return render(request, "story5/add.html", context)

def update(request, pk):
    course = Courses.objects.get(id=pk)
    form = CoursesForm(instance=course)
    if request.method == "POST":
        form = CoursesForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('/')
    context = {
        "form" : form,
        "task" : "update",
    }
    return render(request, 'story5/add.html', context)

def delete(request, pk):
    course = Courses.objects.get(id=pk)
    if request.method == "POST":
        course.delete()
        return redirect('/')

def show(request, pk):
    course = Courses.objects.get(id=pk)
    context = {
        "course" : course
    }
    return render(request, 'story5/courses.html', context)
    