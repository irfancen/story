from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('courses/', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('update/<str:pk>/', views.update, name='update'),
    path('delete/<str:pk>/', views.delete, name='delete'),
    path('show/<str:pk>/', views.show, name='show')
]