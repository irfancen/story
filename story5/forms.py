from django import forms
from . import models

class CoursesForm(forms.ModelForm):
    class Meta:
        model = models.Courses
        fields = ("course", "lecturer", "sks", "desc", "term", "room")

    course = forms.CharField(label='Course Name', widget=forms.TextInput(attrs=
    {
        'type' : 'text',
        'placeholder' : 'Course Name',
        'required' : 'True',
    }))

    lecturer = forms.CharField(label='Lecturer Name', widget=forms.TextInput(attrs=
    {
        'type' : 'text',
        'placeholder' : 'Lecturer Name',
        'required' : 'True',
    }))

    sks = forms.IntegerField(label='Credit per Term', min_value=1, max_value=24, widget=forms.NumberInput(attrs=
    {
        'type' : 'text',
        'required' : 'True',
    }))

    desc = forms.CharField(label='Description', widget=forms.Textarea(attrs=
    {
        'rows' : '4',
        'type' : 'text',
        'placeholder' : 'Description',
        'required' : 'True',
    }))

    term = forms.CharField(label='Term', widget=forms.Select(attrs=
    {
        'required' : 'True',
    }, choices=(
        ("Gasal 2020/2019", "Gasal 2020/2019"),
        ("Genap 2019/2020", "Genap 2019/2020"),
        ("Gasal 2019/2020", "Gasal 2019/2020"),
        ("Lainnya", "Lainnya"),
    )))

    room = forms.CharField(label='Room', widget=forms.TextInput(attrs=
    {
        'type' : 'text',
        'placeholder' : 'Room',
        'required' : 'True',
    }))