from django.db import models

class Courses(models.Model):
    course = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    sks = models.IntegerField()
    desc = models.TextField(max_length=350)
    term = models.CharField(max_length=20)
    room = models.CharField(max_length=10)

    def __str__(self):
        return self.course