from django.shortcuts import render

def index(request):
    return render(request, 'story3/index.html')

def secret(request):
    return render(request, 'story3/secret.html')